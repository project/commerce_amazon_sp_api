<?php

/**
 * @file
 * The install file.
 */

use Drupal\Core\Database\Database;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface;

/**
 * Implements hook_schema().
 */
function commerce_amazon_sp_api_schema() {
  $schema['commerce_amazon_inventory'] = [
    'description' => 'Table used for tracking Amazon inventory',
    'fields' => [
      'id' => [
        'description' => 'Primary Key: id.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'sku' => [
        'description' => 'The seller SKU of the item.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ],
      'fulfillableQuantity' => [
        'description' => 'The item quantity that can be picked, packed, and shipped.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'marketplace' => [
        'description' => 'The marketplace',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'timestamp' => [
        'description' => 'The date and time that any quantity was last updated.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'sku' => ['sku'],
    ],
  ];
  return $schema;
}

/**
 * Add marketplace column.
 *
 * Implements hook_update_N().
 */
function commerce_amazon_sp_api_update_10001(&$sandbox) {
  $spec = commerce_amazon_sp_api_schema();
  $schema = Database::getConnection()->schema();
  $marketplace = $spec['commerce_amazon_inventory']['fields']['marketplace'];
  // Add the new column.
  $schema->addField('commerce_amazon_inventory', 'marketplace', $marketplace);
}

/**
 * Add region field.
 *
 * Implements hook_update_N().
 */
function commerce_amazon_sp_api_update_10002(&$sandbox) {
  $entity_definition_update = \Drupal::entityDefinitionUpdateManager();

  $storage_definition = BaseFieldDefinition::create('list_string')
    ->setLabel(t('AWS region'))
    ->setSetting('allowed_values', AmazonAppInterface::AMAZON_REGIONS)
    ->setDisplayOptions('form', [
      'type' => 'options_buttons',
      'weight' => 4,
    ])
    ->setDisplayConfigurable('form', TRUE);
  $entity_definition_update->installFieldStorageDefinition('region', 'commerce_amazon_app', 'commerce_amazon_sp_api', $storage_definition);
}

/**
 * Add integration field.
 *
 * Implements hook_update_N().
 */
function commerce_amazon_sp_api_update_10003(&$sandbox) {
  $entity_definition_update = \Drupal::entityDefinitionUpdateManager();

  $storage_definition = BaseFieldDefinition::create('map')
    ->setLabel(t('Integration'))
    ->setDescription(t('A serialized array of integration mapping for Commerce core'));
  $entity_definition_update->installFieldStorageDefinition('mapping', 'commerce_amazon_marketplace', 'commerce_amazon_sp_api', $storage_definition);
}

/**
 * Add logging field.
 *
 * Implements hook_update_N().
 */
function commerce_amazon_sp_api_update_10005(&$sandbox) {
  $entity_definition_update = \Drupal::entityDefinitionUpdateManager();

  $storage_definition = BaseFieldDefinition::create('boolean')
    ->setLabel(t('Log requests'))
    ->setDefaultValue(FALSE)
    ->setDescription(t('Log all requests'))
    ->setSetting('on_label', 'Enabled')
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

  $entity_definition_update->installFieldStorageDefinition('logging', 'commerce_amazon_app', 'commerce_amazon_sp_api', $storage_definition);
}

/**
 * Reorganize table.
 *
 * Implements hook_update_N().
 */
function commerce_amazon_sp_api_update_10006(&$sandbox) {
  $spec = commerce_amazon_sp_api_schema();
  $schema = Database::getConnection()->schema();

  $schema->dropTable('commerce_amazon_inventory');
  $schema->createTable('commerce_amazon_inventory', $spec['commerce_amazon_inventory']);
}

<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface;

/**
 * Provides amazon app list.
 */
class AmazonAppListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Name');
    $header['marketplaces'] = $this->t('Marketplaces');
    $header['mode'] = $this->t('Mode');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface $entity */
    $row['label'] = $entity->label();

    $marketplaces = [];
    foreach ($entity->getMarketplaces() as $marketplace) {
      $marketplaces[] = sprintf('%s (%s)', AmazonAppInterface::AMAZON_MARKETPLACES[$marketplace], $marketplace);
    }
    $row['marketplaces'] = implode(', ', $marketplaces);
    $row['mode'] = $entity->getMode() === '0' ? 'Sandbox' : 'Production';
    return $row + parent::buildRow($entity);
  }

}

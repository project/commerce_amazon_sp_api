<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the marketplace storage.
 */
class AmazonMarketplaceStorage extends CommerceContentEntityStorage implements AmazonMarketplaceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadAvailable(OrderInterface $order): array {
    $query = $this->getQuery();
    $query->accessCheck(FALSE);
    $store_condition = $query->orConditionGroup()
      ->notExists('stores')
      ->condition('stores', [$order->getStoreId()], 'IN');
    $query
      ->condition('order_types', [$order->bundle()], 'IN')
      ->condition('status', TRUE)
      ->condition($store_condition)
      ->exists('items');
    $result = $query->execute();

    if (empty($result)) {
      return [];
    }

    return $this->loadMultiple($result);
  }

}

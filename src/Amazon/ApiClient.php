<?php

namespace Drupal\commerce_amazon_sp_api\Amazon;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillmentInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_amazon_sp_api\Event\AmazonEvents;
use Drupal\commerce_amazon_sp_api\Event\AmazonListingsEvent;
use Drupal\commerce_amazon_sp_api\Exception\AmazonApiException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manage api calls against Amazon API.
 */
class ApiClient {

  use LoggerChannelTrait;

  public const AMAZON_AUTH_TOKEN_URL = 'https://api.amazon.com/auth/o2/token';

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * The event dispatcher.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The time.
   */
  protected TimeInterface $time;

  /**
   * The module extension list.
   */
  protected ModuleExtensionList $extensionList;

  /**
   * Api client constructor.
   */
  public function __construct(Client $client, EventDispatcherInterface $event_dispatcher, TimeInterface $time, ModuleExtensionList $extension_list) {
    $this->client = $client;
    $this->eventDispatcher = $event_dispatcher;
    $this->time = $time;
    $this->extensionList = $extension_list;
  }

  /**
   * Get inventory summary for a marketplace.
   */
  public function getInventorySummaries(AmazonMarketplaceInterface $marketplace, $next_token = NULL): array {
    $url = '/fba/inventory/v1/summaries?details=true&granularityType=Marketplace&granularityId=' . $marketplace->getMarketplaceId() . '&marketplaceIds=' . $marketplace->getMarketplaceId();
    if ($next_token) {
      $url .= '&nextToken=' . $next_token;
    }
    return $this->apiCall($url, $marketplace);
  }

  /**
   * Get inventory summary for a single sku.
   */
  public function getInventorySummariesByItem(AmazonItemInterface $item): array {
    $marketplace_id = $item->getAmazonMarketplace()->getMarketplaceId();
    return $this->apiCall(sprintf('/fba/inventory/v1/summaries?details=true&granularityType=Marketplace&granularityId=%s&marketplaceIds=%s&sellerSku=%s', $marketplace_id, $marketplace_id, $item->getSku()), $item->getAmazonMarketplace());
  }

  /**
   * Create inventory item. Sandbox only operation.
   */
  public function createInventoryItem(AmazonItemInterface $item): array {
    $app = $item->getAmazonMarketplace()->getAmazonApp();

    if ($app->isProduction()) {
      return [];
    }

    $payload = [
      'sellerSku' => $item->getSku(),
      'marketplaceId' => $item->getAmazonMarketplace()->getMarketplaceId(),
      'productName' => $item->getPurchasableEntity()->label(),
    ];
    return $this->apiCall('/fba/inventory/v1/items', $item->getAmazonMarketplace(), $payload, 'POST');
  }

  /**
   * Update inventory. Sandbox only operation.
   */
  public function addInventory(AmazonItemInterface $item): array {
    $marketplace = $item->getAmazonMarketplace();
    $app = $marketplace->getAmazonApp();

    if ($app->isProduction()) {
      return [];
    }

    $item_payload = [
      'sellerSku' => $item->getSku(),
      'marketplaceId' => $item->getAmazonMarketplace()->getMarketplaceId(),
      'quantity' => (int) $item->getQuantity(),
    ];

    $payload = ['inventoryItems' => [$item_payload]];
    return $this->apiCall('/fba/inventory/v1/items/inventory', $marketplace, $payload, 'POST');
  }

  /**
   * Create amazon listing.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/listings-items-api-v2021-08-01-reference#putlistingsitem
   */
  public function putListingsItem(AmazonItemInterface $item): array {
    $marketplace = $item->getAmazonMarketplace();
    $app = $marketplace->getAmazonApp();
    $endpoint_url = sprintf(' /inventories/2021-08-01/items/%s/%s?marketplaceIds=%s', $app->getSellerId(), $item->getSku(), $marketplace->getMarketplaceId());
    $payload = [];
    // Trigger event to alter payload.
    $event = new AmazonListingsEvent($item, $payload);
    $this->eventDispatcher->dispatch($event, AmazonEvents::AMAZON_PUT_LISTINGS);
    $payload = $event->getPayload();
    return $this->apiCall($endpoint_url, $marketplace, $payload, 'POST');
  }

  /**
   * Get amazon listing.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/listings-items-api-v2021-08-01-reference#getlistingsitem
   */
  public function getListingItem(AmazonItemInterface $item): array {
    $marketplace = $item->getAmazonMarketplace();
    $app = $marketplace->getAmazonApp();
    $url = sprintf('/inventories/2021-08-01/items/%s/%s?marketplaceIds=%s&issueLocale=en_US&includedData=issues,attributes,summaries,offers,fulfillmentAvailability', $app->getSellerId(), $item->getSku(), $marketplace->getMarketplaceId());
    return $this->apiCall($url, $marketplace);
  }

  /**
   * Get all product types for specific marketplace.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/product-type-definitions-api-v2020-09-01-reference#get-definitions2020-09-01producttypes
   */
  public function searchDefinitionsProductTypes(AmazonMarketplaceInterface $marketplace): array {
    return $this->apiCall('/definitions/2020-09-01/productTypes?marketplaceIds=' . $marketplace->getMarketplaceId(), $marketplace);
  }

  /**
   * Get product types definition for specific marketplace and product type.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/product-type-definitions-api-v2020-09-01-reference#getdefinitionsproducttype
   */
  public function getDefinitionsProductType(AmazonMarketplaceInterface $marketplace, $product_type): array {
    $marketplace_id = $marketplace->getMarketplaceId();
    return $this->apiCall('/definitions/2020-09-01/productTypes/' . $product_type . '?marketplaceIds=' . $marketplace_id, $marketplace);
  }

  /**
   * Create an order.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference#createfulfillmentorder
   */
  public function createFulfillmentOrder(AmazonMarketplaceInterface $marketplace, $payload): array {
    return $this->apiCall('/fba/outbound/2020-07-01/fulfillmentOrders', $marketplace, $payload, 'POST');
  }

  /**
   * Retrieve fulfillment order.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference#getfulfillmentorder
   */
  public function getFulfillmentOrder(AmazonFulfillmentInterface $fulfillment): array {
    $marketplace = $fulfillment->getMarketplace();
    $fulfillment_id = $fulfillment->getFulfillmentId();
    return $this->apiCall('/fba/outbound/2020-07-01/fulfillmentOrders/' . $fulfillment_id, $marketplace);
  }

  /**
   * Create an order.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference#cancelfulfillmentorder
   */
  public function cancelFulfillmentOrder(AmazonFulfillmentInterface $fulfillment): array {
    $marketplace = $fulfillment->getMarketplace();
    return $this->apiCall('/fba/outbound/2020-07-01/fulfillmentOrders/' . $fulfillment->getFulfillmentId() . '/cancel', $marketplace);
  }

  /**
   * List all fulfillment orders.
   */
  public function listAllFulfillmentOrders(AmazonMarketplaceInterface $marketplace): array {
    return $this->apiCall('/fba/outbound/2020-07-01/fulfillmentOrders', $marketplace);
  }

  /**
   * Simulate order transition in sandbox.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference#put-fbaoutbound2020-07-01fulfillmentorderssellerfulfillmentorderidstatus
   */
  public function submitFulfillmentOrderStatusUpdate(AmazonFulfillmentInterface $fulfillment, string $status): array {
    $marketplace = $fulfillment->getMarketplace();
    $app = $marketplace->getAmazonApp();
    if ($app->isProduction()) {
      return [];
    }
    $payload = [
      'fulfillmentOrderStatus' => $status,
    ];

    return $this->apiCall(sprintf('/fba/outbound/2020-07-01/fulfillmentOrders/%s/status', $fulfillment->getFulfillmentId()), $marketplace, $payload, 'PUT');
  }

  /**
   * Get all active marketplaces under account.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/sellers-api-v1-reference#getmarketplaceparticipations
   */
  public function getMarketplaceParticipations(AmazonAppInterface $amazon_app): array {
    return $this->apiCall('/sellers/v1/marketplaceParticipations', $amazon_app);
  }

  /**
   * Helper to create ApiCalls towards Amazon SP API.
   *
   * @param string $endpoint
   *   The endpoint to be used.
   * @param \Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface|\Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface $amazon_entity
   *   The marketplace or app entity.
   * @param array $payload
   *   The payload to be sent.
   * @param string $method
   *   The method to be used.
   *
   * @return array
   *   Return formatted response.
   *
   * @throws \Drupal\commerce_amazon_sp_api\Exception\AmazonApiException|\Random\RandomException
   *   The throws.
   */
  public function apiCall(string $endpoint, AmazonMarketplaceInterface|AmazonAppInterface $amazon_entity, array $payload = [], string $method = 'GET') {
    if ($amazon_entity instanceof AmazonMarketplaceInterface) {
      $amazon_app = $amazon_entity->getAmazonApp();
    }
    else {
      $amazon_app = $amazon_entity;
    }
    // Use region endpoints.
    $base_url = $amazon_app->getEndpointByRegion();

    $commerce_core = $this->extensionList->getExtensionInfo('commerce');
    $values = [
      'headers' => [
        'Content-Type' => 'application/json',
        'x-amz-access-token' => $this->getAccessToken($amazon_app),
        'x-amz-date' => date('Ymd\THis\Z', $this->time->getCurrentTime()),
        'user-agent' => sprintf('Drupal Commerce/%s - https://www.drupal.org/project/commerce_amazon_sp_api (Language=PHP/%s;)', $commerce_core[0]['version'] ?? '2', PHP_VERSION),
      ],
    ];

    if (!$amazon_app->isProduction()) {
      $values['headers']['x-amzn-sandbox'] = 'true';
      $values['headers']['x-amzn-idempotency-token'] = Crypt::hashBase64(random_int(100000, 9999999));
    }

    if (!empty($payload)) {
      $values['json'] = $payload;
    }

    try {
      $request = $this->client->request($method, $base_url . $endpoint, $values);
      $response = Json::decode($request->getBody());
    }
    catch (GuzzleException $exception) {
      if ($errors = $this->formatErrorMessages($exception)) {
        $this->getLogger('commerce_amazon_sp_api')->error(implode("\n", $errors));
      }
      else {
        $this->getLogger('commerce_amazon_sp_api')->error($exception);
      }

      throw new AmazonApiException($exception->getMessage(), $exception->getCode(), $errors, $exception);
    }

    $log_calls = (bool) $amazon_app->get('logging')->value;

    if ($log_calls) {
      $this->getLogger('commerce_amazon_sp_api')->info($request->getBody());
    }

    return $response ?? [];
  }

  /**
   * Format errors.
   */
  protected function formatErrorMessages(GuzzleException $exception): array {
    $data = Json::decode($exception->getResponse()->getBody());

    if (!isset($data['errors'])) {
      return [];
    }

    $error_messages = [];
    foreach ($data['errors'] as $error) {
      $error_messages[] = sprintf('%s: $%s', $error['code'], $error['message']);
    }

    return $error_messages;
  }

  /**
   * Get access token based of existing refresh token.
   */
  public function refreshAccessToken(AmazonAppInterface $amazon_app): ?AmazonAppInterface {
    $payload = [
      'grant_type' => 'refresh_token',
      'refresh_token' => $amazon_app->getRefreshToken(),
      'client_id' => $amazon_app->getClientId(),
      'client_secret' => $amazon_app->getClientSecret(),
    ];

    $headers = [
      'Content-Type' => 'application/x-www-form-urlencoded',
    ];

    try {
      $request = $this->client->post(self::AMAZON_AUTH_TOKEN_URL, ['form_params' => $payload, 'headers' => $headers]);
      $response = Json::decode($request->getBody());
    }
    catch (\Exception $exception) {
      $this->getLogger('commerce_amazon_sp_api')->error($exception);
      throw new \Exception($exception->getMessage(), $exception->getCode());
    }

    // Store token info.
    $amazon_app->setRefreshToken($response['refresh_token']);
    $amazon_app->setAccessToken($response['access_token']);
    // Store expiration one minute less.
    $amazon_app->setExpiresIn($this->time->getCurrentTime() + $response['expires_in'] - 60);

    // Refresh marketplaces.
    try {
      $participations = $this->getMarketplaceParticipations($amazon_app);
      if (isset($participations['payload'])) {
        $marketplaces = [];
        foreach ($participations['payload'] as $participation) {
          $marketplaces[] = $participation['marketplace']['id'];
        }
        $amazon_app->setMarketplaces($marketplaces);
      }
    }
    catch (\Exception $exception) {
      $this->getLogger('commerce_amazon_sp_api')->error($exception);
    }

    $amazon_app->save();

    return $amazon_app;
  }

  /**
   * Retrieve access token.
   */
  public function getAccessToken(AmazonAppInterface $amazon_app): string {
    if ($this->time->getCurrentTime() < $amazon_app->getExpiresIn()) {
      return $amazon_app->getAccessToken();
    }
    $amazon_app = $this->refreshAccessToken($amazon_app);
    return $amazon_app->getAccessToken();
  }

}

<?php

namespace Drupal\commerce_amazon_sp_api\Amazon;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillmentInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_amazon_sp_api\Event\AmazonEvents;
use Drupal\commerce_amazon_sp_api\Event\AmazonFulfillmentOrderCreate;
use Drupal\commerce_amazon_sp_api\Event\AmazonFulfillmentOrderItemValidation;
use Drupal\commerce_amazon_sp_api\Exception\AmazonApiException;
use Drupal\commerce_order\Entity\OrderInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Fulfillment order manager.
 */
class FulfillmentOrder {

  use LoggerChannelTrait;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The api client.
   */
  protected ApiClient $client;

  /**
   * The event dispatcher.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The order constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ApiClient $client, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->client = $client;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Create fulfillment order.
   */
  public function createOrder(OrderInterface $order, AmazonMarketplaceInterface $marketplace): bool {
    // Validate order eligibility if all items are mapped and in stock.
    $items = $this->validateOrderItemsEligibility($order, $marketplace);

    if (!$items) {
      return FALSE;
    }

    $commerce_amazon_fulfillment_storage = $this->entityTypeManager->getStorage('commerce_amazon_fulfillment');
    $commerce_log_storage = $this->entityTypeManager->getStorage('commerce_log');

    $profiles = $order->collectProfiles();

    $destination_address = [];
    if (isset($profiles['shipping'])) {
      $shipping_profile = $profiles['shipping'];
      /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $address */
      $address = $shipping_profile->get('address')->first();
      $full_name_parts = [
        $address->getGivenName(),
        $address->getFamilyName(),
      ];
      $destination_address = [
        'addressLine1' => $address->getAddressLine1(),
        'countryCode' => $address->getCountryCode(),
        'stateOrRegion' => $address->getAdministrativeArea(),
        'name' => implode(' ', $full_name_parts),
        'postalCode' => $address->getPostalCode(),
        'addressLine2' => $address->getAddressLine2(),
        'addressLine3' => $address->getAddressLine3(),
        'city' => $address->getLocality(),
        'districtOrCountry' => $address->getDependentLocality(),
        'phone' => $shipping_profile->hasField('field_phone') ? $shipping_profile->get('field_phone')->value : '',
      ];

      foreach ($destination_address as $key => $value) {
        if (empty($value)) {
          unset($destination_address[$key]);
        }
      }
    }

    if (empty($destination_address)) {
      $commerce_log_storage->generate($order, 'order_not_eligible', ['message' => 'Missing shipping address'])->save();
      return FALSE;
    }

    $payload = [
      'sellerFulfillmentOrderId' => 'AMA-DC-' . $order->id(),
      'marketplaceId' => $marketplace->getMarketplaceId(),
      'displayableOrderId' => $order->getOrderNumber(),
      'displayableOrderDate' => date('c', $order->getPlacedTime() ?? $order->getChangedTime()),
      'displayableOrderComment' => $order->getOrderNumber(),
      'shippingSpeedCategory' => $marketplace->getFulfillmentShippingSpeed(),
      'destinationAddress' => $destination_address,
      'fulfillmentAction' => 'Ship',
      'fulfillmentPolicy' => $marketplace->getFulfillmentPolicy(),
      'items' => $items,
      'notificationEmails' => [
        $order->getEmail(),
      ],
    ];

    $event = new AmazonFulfillmentOrderCreate($order, $payload, $marketplace);
    $this->eventDispatcher->dispatch($event, AmazonEvents::AMAZON_FULFILLMENT_CREATE_ORDER);
    $payload = $event->getPayload();

    try {
      $this->client->createFulfillmentOrder($marketplace, $payload);
    }
    catch (AmazonApiException $exception) {
      $this->getLogger('commerce_amazon_sp_api')->error($exception->getMessage());
      if ($errors = $exception->getErrors()) {
        $commerce_log_storage->generate($order, 'order_not_eligible', ['message' => implode("\n", $errors)])->save();
      }
      else {
        $commerce_log_storage->generate($order, 'order_not_eligible', ['message' => sprintf('Communication failure with API. HTTP Response code %s from Amazon', $exception->getCode())])->save();
      }
      return FALSE;
    }

    $fulfillment_item = $commerce_amazon_fulfillment_storage->create([
      // Allow storing altered default pattern.
      'fulfillment_id' => $payload['sellerFulfillmentOrderId'],
      'order' => $order->id(),
      'marketplace' => $marketplace->id(),
      'state' => 'new',
    ]);
    $fulfillment_item->save();

    $commerce_log_storage->generate($order, 'fulfillment_order_created', ['fulfillment_id' => $fulfillment_item->id()])->save();

    $order = $this->client->getFulfillmentOrder($fulfillment_item);

    $status = $order['payload']['fulfillmentOrder']['fulfillmentOrderStatus'] ?? NULL;

    if ($status) {
      $fulfillment_item->getState()->applyTransitionById(strtolower($status));
      $fulfillment_item->save();
    }

    return TRUE;
  }

  /**
   * Validate that all products are mapped and in stock.
   */
  public function validateOrderItemsEligibility(OrderInterface $order, AmazonMarketplaceInterface $marketplace): array {
    $commerce_amazon_item_storage = $this->entityTypeManager->getStorage('commerce_amazon_item');
    $commerce_log_storage = $this->entityTypeManager->getStorage('commerce_log');

    $items = [];

    foreach ($order->getItems() as $item) {
      $event = new AmazonFulfillmentOrderItemValidation($item, $marketplace, $commerce_amazon_item_storage, $commerce_log_storage);
      $this->eventDispatcher->dispatch($event, AmazonEvents::AMAZON_FULFILLMENT_ORDER_ITEM_VALIDATION);

      $order_item_validated = $event->getItem();

      if (!$order_item_validated) {
        return [];
      }

      // Check if we push single or multiple items.
      if (isset($order_item_validated['sellerFulfillmentOrderItemId'])) {
        $items[] = $order_item_validated;
      }
      else {
        $items = array_merge($items, $order_item_validated);
      }

    }

    return $items;
  }

  /**
   * Update open orders.
   */
  public function syncFulfillments(AmazonMarketplaceInterface $marketplace): void {
    $open_fulfillments = $this->entityTypeManager->getStorage('commerce_amazon_fulfillment')->loadOpenFulfillmentIds($marketplace);

    foreach (array_chunk($open_fulfillments, 10, TRUE) as $ids) {
      $fulfillments = $this->entityTypeManager->getStorage('commerce_amazon_fulfillment')->loadMultiple($ids);

      foreach ($fulfillments as $fulfillment) {
        $this->syncFulfillment($fulfillment);
      }
    }
  }

  /**
   * Update fulfillment.
   */
  public function syncFulfillment(AmazonFulfillmentInterface $fulfillment): bool {
    try {
      $response = $this->client->getFulfillmentOrder($fulfillment);
      $status = $response['payload']['fulfillmentOrder']['fulfillmentOrderStatus'] ? strtolower($response['payload']['fulfillmentOrder']['fulfillmentOrderStatus']) : NULL;
      $shipments = $response['payload']['fulfillmentShipments'] ?? [];
    }
    catch (\Exception $exception) {
      $this->getLogger('commerce_amazon_sp_api')->error($exception->getMessage());
      return FALSE;
    }

    $commerce_log_storage = $this->entityTypeManager->getStorage('commerce_log');

    $save_fulfillment = FALSE;
    if ($shipments) {
      $fulfillment->setShipments($shipments);
      $save_fulfillment = TRUE;
    }
    if ($status && $status !== $fulfillment->getState()->getId()) {
      $commerce_log_storage->generate($fulfillment->getOrder(), 'fulfillment_order_transition', [
        'fulfillment_id' => $fulfillment->id(),
        'from_state' => $fulfillment->getState()->getId(),
        'to_state' => $status,
      ])->save();

      $fulfillment->getState()->applyTransitionById(strtolower($status));
      $save_fulfillment = TRUE;
    }

    if ($save_fulfillment) {
      $fulfillment->save();
    }

    return TRUE;
  }

}

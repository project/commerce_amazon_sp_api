<?php

namespace Drupal\commerce_amazon_sp_api\Amazon;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonItem;
use Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_amazon_sp_api\Event\AmazonEvents;
use Drupal\commerce_amazon_sp_api\Event\AmazonItemSync;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Handling inventory tasks.
 */
class Inventory {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The database.
   */
  protected Connection $database;

  /**
   * The api client.
   */
  protected ApiClient $client;

  /**
   * The event dispatcher.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The constructor.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, ApiClient $client, EventDispatcherInterface $event_dispatcher,) {
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
    $this->client = $client;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Sync inventory.
   */
  public function sync(AmazonMarketplaceInterface $marketplace): void {
    $inventory_summaries = $this->getInventorySummaries($marketplace);
    foreach ($inventory_summaries as $inventory_summary) {
      $sku = $inventory_summary['sellerSku'];
      $fulfillable_quantity = $inventory_summary['inventoryDetails']['fulfillableQuantity'];
      $this->updateBySku($sku, $marketplace, $fulfillable_quantity);
      if ($marketplace->syncItems()) {
        $this->syncItem($sku, $marketplace);
      }
    }
  }

  /**
   * Get all quantities.
   */
  protected function getInventorySummaries(AmazonMarketplaceInterface $marketplace): array {
    $inventory_summaries = [];
    $next_token = NULL;
    do {
      $response = $this->client->getInventorySummaries($marketplace, $next_token);
      $next_token = $response['pagination']['nextToken'] ?? NULL;
      $inventory_summaries = array_merge($inventory_summaries, $response['payload']['inventorySummaries'] ?? []);
      // There is limit of 2 req/s.
      sleep(1);
    } while ($next_token);

    return $inventory_summaries;
  }

  /**
   * Retrieve stock by sku.
   */
  public function getBySku($sku, AmazonMarketplaceInterface $marketplace): int {
    return (int) $this->database->select('commerce_amazon_inventory', 'i')
      ->fields('i', ['fulfillableQuantity'])
      ->condition('sku', $sku)
      ->condition('marketplace', $marketplace->id())
      ->execute()->fetchField();
  }

  /**
   * Retrieve stock by sku.
   */
  public function getByItem(AmazonItemInterface $item): int {
    return (int) $this->database->select('commerce_amazon_inventory', 'i')
      ->fields('i', ['fulfillableQuantity'])
      ->condition('sku', $item->getSku())
      ->condition('marketplace', $item->getAmazonMarketplaceId())
      ->execute()->fetchField();
  }

  /**
   * Update fulfillable quantity.
   */
  public function updateBySku($sku, AmazonMarketplaceInterface $marketplace, $quantity): void {
    $this->database->merge('commerce_amazon_inventory')
      ->keys(['sku' => $sku, 'marketplace' => $marketplace->id()])
      ->fields(['fulfillableQuantity' => $quantity, 'timestamp' => time()])
      ->execute();
  }

  /**
   * Sync item.
   */
  public function syncItem($sku, AmazonMarketplaceInterface $marketplace): void {
    $amazon_items = $this->entityTypeManager->getStorage('commerce_amazon_item')
      ->loadByProperties(['sku' => $sku, 'marketplace' => $marketplace->id()]);
    if (empty($amazon_items)) {
      $variations = $this->entityTypeManager->getStorage('commerce_product_variation')->loadByProperties(['sku' => $sku]);
      $payload = [
        'sku' => $sku,
        'marketplace' => $marketplace->id(),
        'purchasable_entity' => $variations ? end($variations) : NULL,
      ];
      // Trigger event to alter payload.
      $event = new AmazonItemSync($sku, $payload, $marketplace);
      $this->eventDispatcher->dispatch($event, AmazonEvents::AMAZON_ITEM_SYNC);
      $payload = $event->getPayload();

      // Create only item if purchase entity exists.
      if (!empty($payload['purchasable_entity'])) {
        $item = AmazonItem::create($payload);
        $item->skipUpdate = TRUE;
        $item->save();
      }
    }
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the fulfillment entity type.
 */
final class AmazonFulfillmentListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['order'] = $this->t('Order');
    $header['marketplace'] = $this->t('Marketplace');
    $header['state'] = $this->t('State');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillmentInterface $entity */
    $row['id'] = $entity->id();
    $row['order'] = $entity->getOrder();
    $row['marketplace'] = $entity->getMarketplace();
    $row['state'] = $entity->getState();
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}

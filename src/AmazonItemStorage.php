<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;

/**
 * Defines the fulfillment storage.
 */
class AmazonItemStorage extends CommerceContentEntityStorage implements AmazonItemStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByProductVariation(PurchasableEntityInterface $product_variation, ?AmazonMarketplaceInterface $marketplace): ?AmazonItemInterface {
    $query = $this->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', TRUE)
      ->condition('purchasable_entity', $product_variation->id());

    if ($marketplace) {
      $query->condition('marketplace', $marketplace->id());
    }
    $result = $query->execute();

    return $result ? $this->load(end($result)) : NULL;
  }

}

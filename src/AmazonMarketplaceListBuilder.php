<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface;

/**
 * Provides a list controller for the marketplace entity type.
 */
final class AmazonMarketplaceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['marketplace'] = $this->t('Marketplace');
    $header['mode'] = $this->t('Mode');
    $header['status'] = $this->t('Status');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['changed'] = $this->t('Updated');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = Link::createFromRoute($entity->label(), 'entity.commerce_amazon_marketplace.canonical', ['commerce_amazon_marketplace' => $entity->id()]);
    $row['marketplace'] = sprintf('%s (%s)', AmazonAppInterface::AMAZON_MARKETPLACES[$entity->getMarketplaceId()], $entity->getMarketplaceId());
    $row['mode'] = $entity->getAmazonApp()->getMode() === '0' ? 'Sandbox' : 'Production';
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    $row['created']['data'] = $entity->get('created')->view(['label' => 'hidden']);
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    return $row + parent::buildRow($entity);
  }

}

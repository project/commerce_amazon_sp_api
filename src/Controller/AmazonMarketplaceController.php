<?php

namespace Drupal\commerce_amazon_sp_api\Controller;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;

/**
 * Title controller.
 */
class AmazonMarketplaceController {

  use StringTranslationTrait;

  /**
   * Provides the title callback for the items collection route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return string
   *   The title.
   */
  public function amazonMarketplaceTitle(RouteMatchInterface $route_match) {
    $amazon_marketplace = $route_match->getParameter('commerce_amazon_marketplace');
    assert($amazon_marketplace instanceof AmazonMarketplaceInterface);
    return $this->t('Items for %label', ['%label' => $amazon_marketplace->label()]);
  }

}

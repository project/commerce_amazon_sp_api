<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the item entity type.
 */
final class AmazonItemListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['seller_sku'] = $this->t('Seller SKU');
    $header['fulfillable_quantity'] = $this->t('Fulfillable Quantity');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface $entity */
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['seller_sku'] = $entity->getSku();
    $row['fulfillable_quantity'] = $entity->getFulfillableQuantity();
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');

    return $row + parent::buildRow($entity);
  }

}

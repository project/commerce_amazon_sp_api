<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillmentInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the fulfillment storage.
 */
class AmazonFulfillmentStorage extends CommerceContentEntityStorage implements AmazonFulfillmentStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByOrder(OrderInterface $order): ?AmazonFulfillmentInterface {
    $query = $this->getQuery()
      ->accessCheck()
      ->condition('order', $order->id());
    $result = $query->execute();

    return $result ? $this->load(end($result)) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOpenFulfillmentIds(AmazonMarketplaceInterface $marketplace): array {
    $query = $this->getQuery()
      ->accessCheck()
      ->condition('marketplace', $marketplace->id())
      // Skip cancelled, completed and new. The new status only indicates
      // that is created in Drupal. Check only status which can progress.
      ->condition('state', ['new', 'received', 'planning', 'processing'], 'IN');
    $result = $query->execute();

    return $result ?? [];
  }

}

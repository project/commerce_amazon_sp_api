<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;

/**
 * Provides interface for AmazonItemStorageInterface.
 */
interface AmazonItemStorageInterface {

  /**
   * Load by product variation.
   */
  public function loadByProductVariation(PurchasableEntityInterface $product_variation, ?AmazonMarketplaceInterface $marketplace): ?AmazonItemInterface;

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Exception;

/**
 * Defines class AmazonApiException.
 */
final class AmazonApiException extends \Exception {

  /**
   * Errors.
   *
   * @var array
   */

  protected array $errors = [];

  /**
   * Constructor.
   */
  public function __construct(
    string $message = '',
    int $code = 0,
    array $errors = [],
    ?\Throwable $previousException = NULL,
  ) {
    parent::__construct($message, $code, $previousException);
    $this->errors = $errors;
  }

  /**
   * Gets the HTTP body of the server response either as array.
   */
  public function getErrors(): array {
    return $this->errors;
  }

}

<?php

namespace Drupal\commerce_amazon_sp_api\Event;

/**
 * Provides list of events.
 */
final class AmazonEvents {

  /**
   * Name of the event fired before payload is sent to Amazon.
   *
   * @Event
   *
   * @see \Drupal\commerce_amazon_sp_api\Event\AmazonListingsEvent
   */
  const AMAZON_PUT_LISTINGS = 'amazon_put_listings';

  /**
   * Name of the event fired before item is created.
   *
   * @Event
   *
   * @see \Drupal\commerce_amazon_sp_api\Event\AmazonItemSync
   */
  const AMAZON_ITEM_SYNC = 'amazon_item_sync';

  /**
   * Name of the event fired before order is created.
   *
   * @Event
   *
   * @see \Drupal\commerce_amazon_sp_api\Event\AmazonFulfillmentOrderCreate
   */
  const AMAZON_FULFILLMENT_CREATE_ORDER = 'amazon_fulfillment_create_order';

  /**
   * Name of the event fired during order item validation is run.
   *
   * @Event
   *
   * @see \Drupal\commerce_amazon_sp_api\Event\AmazonFulfillmentOrderItemValidation
   */
  const AMAZON_FULFILLMENT_ORDER_ITEM_VALIDATION = 'amazon_fulfillment_order_item_validation';

}

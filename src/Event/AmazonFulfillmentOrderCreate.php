<?php

namespace Drupal\commerce_amazon_sp_api\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Event that is fired before creating new fulfillment order.
 *
 * @package Drupal\commerce_amazon_sp_api\Event
 */
class AmazonFulfillmentOrderCreate extends Event {

  /**
   * The order.
   */
  public OrderInterface $order;

  /**
   * The payload.
   */
  public array $payload;

  /**
   * The Amazon marketplace.
   */
  public AmazonMarketplaceInterface $marketplace;

  /**
   * Constructs a new AmazonFulfillmentOrderCreate object.
   */
  public function __construct(OrderInterface $order, array $payload, AmazonMarketplaceInterface $marketplace) {
    $this->order = $order;
    $this->payload = $payload;
    $this->marketplace = $marketplace;
  }

  /**
   * Get the sku.
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

  /**
   * Set the payload.
   */
  public function setPayload(array $payload): static {
    $this->payload = $payload;
    return $this;
  }

  /**
   * Get the payload.
   */
  public function getPayload(): array {
    return $this->payload;
  }

  /**
   * Get the marketplace.
   */
  public function getMarketplace(): AmazonMarketplaceInterface {
    return $this->marketplace;
  }

}

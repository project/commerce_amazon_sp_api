<?php

namespace Drupal\commerce_amazon_sp_api\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface;

/**
 * Event that is fired before sending product listings payload.
 *
 * @package Drupal\commerce_amazon_sp_api\Event
 */
class AmazonListingsEvent extends Event {

  /**
   * The Amazon item.
   */
  public AmazonItemInterface $item;

  /**
   * The payload.
   */
  public array $payload;

  /**
   * Constructs a new AmazonListingsEvent object.
   */
  public function __construct(AmazonItemInterface $item, array $payload) {
    $this->item = $item;
    $this->payload = $payload;
  }

  /**
   * Get the item.
   */
  public function getItem(): AmazonItemInterface {
    return $this->item;
  }

  /**
   * Set the payload.
   */
  public function setPayload(array $payload): static {
    $this->payload = $payload;
    return $this;
  }

  /**
   * Get the payload.
   */
  public function getPayload(): array {
    return $this->payload;
  }

}

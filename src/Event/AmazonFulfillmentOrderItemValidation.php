<?php

namespace Drupal\commerce_amazon_sp_api\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_amazon_sp_api\AmazonItemStorageInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_log\LogStorageInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;

/**
 * Event that is fired before each order item validation.
 *
 * @package Drupal\commerce_amazon_sp_api\Event
 */
class AmazonFulfillmentOrderItemValidation extends Event {

  /**
   * The order.
   */
  public OrderItemInterface $orderItem;

  /**
   * The payload.
   */
  public array $item;

  /**
   * The Amazon marketplace.
   */
  public AmazonMarketplaceInterface $marketplace;

  /**
   * The amazon item storage.
   */
  public AmazonItemStorageInterface $itemStorage;

  /**
   * The log storage.
   */
  public LogStorageInterface $logStorage;

  /**
   * Flag if we need to skip default validation.
   */
  public bool $skipValidation;

  /**
   * Constructs a new AmazonFulfillmentOrderItemValidation object.
   */
  public function __construct(OrderItemInterface $order_item, AmazonMarketplaceInterface $marketplace, AmazonItemStorageInterface $item_storage, LogStorageInterface $log_storage) {
    $this->orderItem = $order_item;
    $this->marketplace = $marketplace;
    $this->itemStorage = $item_storage;
    $this->logStorage = $log_storage;
  }

  /**
   * Get the sku.
   */
  public function getOrderItem(): OrderItemInterface {
    return $this->orderItem;
  }

  /**
   * Set the payload.
   */
  public function setItem(array $item): static {
    $this->item = $item;
    return $this;
  }

  /**
   * Get the payload.
   */
  public function getItem(): array {
    if (!$this->skipValidation()) {
      return $this->validateOrderItem();
    }
    return $this->item;
  }

  /**
   * Set validation.
   */
  public function setSkipValidation(bool $skipValidation): static {
    $this->skipValidation = $skipValidation;
    return $this;
  }

  /**
   * Get skip validation.
   */
  public function skipValidation(): bool {
    return $this->skipValidation ?? FALSE;
  }

  /**
   * Get the marketplace.
   */
  public function getMarketplace(): AmazonMarketplaceInterface {
    return $this->marketplace;
  }

  /**
   * Validate order item and create payload.
   */
  public function validateOrderItem(): array {
    $item = $this->getOrderItem();
    $amazon_item = $this->itemStorage->loadByProductVariation($item->getPurchasedEntity(), $this->getMarketplace());

    if (!$amazon_item) {
      $this->logStorage->generate($item->getOrder(), 'order_not_eligible', ['message' => sprintf('Drupal SKU %s is not mapped', $item->getPurchasedEntity()->getSku())])->save();
      return [];
    }

    if ($amazon_item->getFulfillableQuantity() < (int) $item->getQuantity()) {
      $this->logStorage->generate($item->getOrder(), 'order_not_eligible', ['message' => sprintf('Seller sku %s does not have required quantity', $amazon_item->getSku())])->save();
      return [];
    }

    if ($this->getMarketplace()->getLowInventoryThreshold() >= $amazon_item->getFulfillableQuantity()) {
      $this->logStorage->generate($item->getOrder(), 'order_not_eligible', ['message' => sprintf('Seller sku %s is below defined low inventory threshold', $amazon_item->getSku())])->save();
      return [];
    }

    return [
      'quantity' => (int) $item->getQuantity(),
      'sellerSku' => $amazon_item->getSku(),
      'sellerFulfillmentOrderItemId' => $item->id(),
    ];
  }

}

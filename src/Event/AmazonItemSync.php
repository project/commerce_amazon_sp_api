<?php

namespace Drupal\commerce_amazon_sp_api\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;

/**
 * Event that is fired before create new item.
 *
 * @package Drupal\commerce_amazon_sp_api\Event
 */
class AmazonItemSync extends Event {

  /**
   * The Amazon Seller sku.
   */
  public string $sku;

  /**
   * The payload.
   */
  public array $payload;

  /**
   * The Amazon marketplace.
   */
  public AmazonMarketplaceInterface $marketplace;

  /**
   * Constructs a new AmazonItemSync object.
   */
  public function __construct(string $sku, array $payload, AmazonMarketplaceInterface $marketplace) {
    $this->sku = $sku;
    $this->payload = $payload;
    $this->marketplace = $marketplace;
  }

  /**
   * Get the sku.
   */
  public function getSku(): string {
    return $this->sku;
  }

  /**
   * Set the payload.
   */
  public function setPayload(array $payload): static {
    $this->payload = $payload;
    return $this;
  }

  /**
   * Get the payload.
   */
  public function getPayload(): array {
    return $this->payload;
  }

  /**
   * Get the marketplace.
   */
  public function getMarketplace(): AmazonMarketplaceInterface {
    return $this->marketplace;
  }

}

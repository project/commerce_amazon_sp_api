<?php

namespace Drupal\commerce_amazon_sp_api\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_amazon_sp_api\Amazon\ApiClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Amazon app default form.
 *
 * @property \Drupal\commerce_amazon_sp_api\Entity\AmazonAppInterface $entity
 */
final class AmazonAppForm extends ContentEntityForm {

  /**
   * The client.
   */
  protected ApiClient $amazonClient;

  /**
   * Construct Amazon merchant form.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, ApiClient $amazon_client) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->amazonClient = $amazon_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('commerce_amazon_sp_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['refresh_token']['#access'] = $this->entity->isNew();
    $form['marketplaces']['#access'] = !$this->entity->isNew();
    $form['marketplaces']['#disabled'] = TRUE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $form_state->setRedirect('entity.commerce_amazon_app.collection');
    $this->amazonClient->refreshAccessToken($this->entity);
    return $result;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Form controller for the item forms.
 */
final class AmazonItemForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit %label', ['%label' => $this->entity->label()]);
    }

    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface $item */
    $item = $this->entity;

    if ($item->getAmazonMarketplace()->getAmazonApp()->isProduction()) {
      $form['quantity']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if ($route_match->getRawParameter($entity_type_id) !== NULL) {
      $entity = $route_match->getParameter($entity_type_id);
    }
    elseif ($product_variation = $route_match->getParameter('commerce_product_variation')) {
      $values = [
        'type' => 'commerce_product_variation',
        'purchasable_entity' => $product_variation->id(),
      ];
      $entity = $this->entityTypeManager->getStorage($entity_type_id)
        ->create($values);
    }
    else {
      $amazon_marketplace = $route_match->getParameter('commerce_amazon_marketplace');
      $values = [
        'marketplace' => $amazon_marketplace->id(),
      ];
      $entity = $this->entityTypeManager->getStorage($entity_type_id)
        ->create($values);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $save = $this->entity->save();
    $this->messenger()
      ->addMessage($this->t('Saved the %label item.', ['%label' => $this->entity->label()]));
    $form_state->setRedirectUrl(Url::fromRoute('entity.commerce_amazon_marketplace.canonical', ['commerce_amazon_marketplace' => $this->entity->getAmazonMarketplaceId()]));
    return $save;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormDisplay(EntityFormDisplayInterface $form_display, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface $item */
    $item = $this->entity;
    if ($item->isNew() &&
      is_null($item->getAmazonMarketplaceId()) &&
      $item->getPurchasableEntityId()) {
      $form_display->setComponent('marketplace', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -10,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ]);
      $form_display->removeComponent('purchasable_entity');
    }
    $form_state->set('form_display', $form_display);
    return $this;
  }

}

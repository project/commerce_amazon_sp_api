<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for a marketplace mapping.
 */
final class AmazonMarketplaceIntegrationForm extends ContentEntityForm {

  /**
   * The workflow manager.
   */
  protected WorkflowManagerInterface $workflowManager;

  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, WorkflowManagerInterface $workflow_manager) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->workflowManager = $workflow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.workflow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface $marketplace */
    $marketplace = $this->entity;

    $integration_details = $marketplace->getMapping();

    $all_order_types = $this->entityTypeManager->getStorage('commerce_order_type')
      ->loadMultiple();

    $order_types = $marketplace->getOrderTypes() ?? $all_order_types;
    $form['fulfillment'] = [
      '#type' => 'details',
      '#title' => $this->t('Fulfillment integration'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    foreach ($order_types as $order_type) {
      $machine_name = $order_type->id();

      $form['fulfillment']['order_types'][$machine_name] = [
        '#type' => 'details',
        '#title' => $this->t('Order type: @order_type', ['@order_type' => $order_type->label()]),
        '#open' => FALSE,
      ];

      $form['fulfillment']['order_types'][$machine_name]['workflow'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable automatic workflow integration'),
        '#description' => $this->t('Enable this option if you want to automate order transitions based on fulfillment order state.'),
        '#default_value' => $integration_details['fulfillment']['order_types'][$machine_name]['workflow'] ?? FALSE,
      ];

      $order_workflow = $this->workflowManager->getDefinition($order_type->getWorkflowId());
      $order_transitions = $order_workflow['transitions'] ?? [];

      // Skip order place.
      unset($order_transitions['place']);

      $order_transitions_options = array_map(function (array $transition) {
        return $transition['label'];
      }, $order_transitions);

      $fulfillment_workflow = $this->workflowManager->getDefinition('fulfillment_default');
      $fulfillment_transitions = $fulfillment_workflow['transitions'] ?? [];

      foreach ($fulfillment_transitions as $transition_id => $fulfillment_transition) {
        $form['fulfillment']['order_types'][$machine_name][$transition_id] = [
          '#type' => 'select',
          '#title' => $fulfillment_transition['label'],
          '#description' => $this->t('Order transition to trigger if fulfillment states transition from @start to @end', [
            '@start' => implode(',', $fulfillment_transition['from']),
            '@end' => $fulfillment_transition['to'],
          ]),
          '#options' => $order_transitions_options,
          '#default_value' => $integration_details['fulfillment']['order_types'][$machine_name][$transition_id] ?? NULL,
          '#states' => [
            'visible' => [
              ':input[name="fulfillment[order_types][' . $machine_name . '][workflow]"]' => ['checked' => TRUE],
            ],
          ],
        ];
      }
    }

    $form['fulfillment']['fulfillment_policy'] = [
      '#type' => 'select',
      '#title' => $this->t('Fulfillment Policy'),
      '#description' => $this->t('See more about fulfillment policy <a href="@link">here</a>', ['@link' => 'https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference#createfulfillmentorderitemlist']),
      '#options' => [
        'FillOrKill' => 'FillOrKill',
        'FillAll' => 'FillAll',
        'FillAllAvailable' => 'FillAllAvailable',
        'none' => 'Custom integration',
      ],
      '#default_value' => $integration_details['fulfillment']['fulfillment_policy'] ?? 'FillOrKill',
    ];

    $form['fulfillment']['shipping_speed_category'] = [
      '#type' => 'select',
      '#title' => $this->t('Shipping speed category'),
      '#description' => $this->t('See more about fulfillment shipping speed <a href="@link">here</a>', ['@link' => 'https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference#shippingspeedcategory']),
      '#options' => [
        'Standard' => 'Standard',
        'Expedited' => 'Expedited',
        'Priority' => 'Priority',
        'none' => 'Custom integration',
      ],
      '#default_value' => $integration_details['fulfillment']['shipping_speed_category'] ?? 'Standard',
    ];

    $form['fulfillment']['inventory_threshold'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Low inventory threshold'),
      '#description' => $this->t("The number considered as low threshold where it's not anymore for order allowed to be send to Amazon for fulfillment."),
      '#default_value' => $integration_details['fulfillment']['inventory_threshold'] ?? 0,
    ];

    $form['inventory'] = [
      '#type' => 'details',
      '#title' => $this->t('Inventory integration'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['inventory']['sync_period'] = [
      '#type' => 'select',
      '#title' => $this->t('Sync period'),
      '#options' => [
        600 => $this->t('10 minutes'),
        900 => $this->t('15 minutes'),
        1800 => $this->t('30 minutes'),
        3600 => $this->t('1 hour'),
      ],
      '#default_value' => $integration_details['inventory']['sync_period'] ?? 900,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $data = [
      'fulfillment' => $form_state->getValue('fulfillment') ?? [],
      'inventory' => $form_state->getValue('inventory') ?? [],
      'logging' => $form_state->getValue('inventory') ?? 0,
    ];
    $this->entity->setMapping($data);
    $this->entity->save();
    $this->messenger()->addStatus($this->t('The configuration has been updated.'));
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\entity\Form\EntityDuplicateFormTrait;

/**
 * Form controller for the marketplace entity edit forms.
 */
final class AmazonMarketplaceForm extends ContentEntityForm {

  use EntityDuplicateFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Skip building the form if there are no available stores.
    $store_query = $this->entityTypeManager->getStorage('commerce_amazon_app')->getQuery()->accessCheck(TRUE);
    if ($store_query->count()->execute() == 0) {
      $link = Link::createFromRoute('Add a new app.', 'entity.commerce_amazon_app.add_form');
      $form['warning'] = [
        '#markup' => $this->t("Amazon Marketplace can't be created until an Amazon App has been added. @link", ['@link' => $link->toString()]),
      ];
      return $form;
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['#theme'] = ['commerce_amazon_marketplace_form'];
    $form['#attached']['library'][] = 'commerce_amazon_sp_api/form';

    $form['advanced'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['entity-meta']],
      '#weight' => 99,
    ];
    $form['option_details'] = [
      '#type' => 'container',
      '#title' => $this->t('Options'),
      '#group' => 'advanced',
      '#attributes' => ['class' => ['entity-meta__header']],
      '#weight' => -100,
    ];

    $form['amazon_details'] = [
      '#type' => 'container',
      '#title' => $this->t('Amazon settings'),
      '#group' => 'advanced',
      '#attributes' => ['class' => ['entity-meta__header']],
      '#weight' => -100,
    ];

    $field_details_mapping = [
      'status' => 'option_details',
      'amazon_app' => 'amazon_details',
      'marketplace_id' => 'amazon_details',
      'sync_items' => 'amazon_details',
      'order_types' => 'option_details',
      'stores' => 'option_details',
    ];
    foreach ($field_details_mapping as $field => $group) {
      if (isset($form[$field])) {
        $form[$field]['#group'] = $group;
      }
    }

    $form['marketplace_id']['#prefix'] = '<div id="marketplace-unique-wrapper">';
    $form['marketplace_id']['#suffix'] = '</div>';

    if (empty($this->entity->getMarketplaceId())) {
      $form['marketplace_id']['#disabled'] = TRUE;
    }
    else {
      $amazon_app_id = $form_state->getValue(['amazon_app', 0, 'target_id']);
      $form['marketplace_id']['#disabled'] = empty($amazon_app_id);

      if ($amazon_app_id) {
        $amazon_app = $this->entityTypeManager->getStorage('commerce_amazon_app')->load($amazon_app_id);
        $marketplaces = $amazon_app->getMarketplaces();
        $options = $form['marketplace_id']['widget']['#options'];

        foreach ($options as $id => $option) {
          if (!in_array($id, $marketplaces)) {
            unset($options[$id]);
          }
        }
        $form['marketplace_id']['widget']['#options'] = $options;
      }
    }

    $form['amazon_app']['widget']['#ajax'] = [
      'callback' => '::ajaxRefresh',
      'wrapper' => 'marketplace-unique-wrapper',
    ];

    if (!$this->entity->get('amazon_app')->isEmpty() && !$this->entity->get('marketplace_id')->isEmpty()) {
      $form['marketplace_id']['#disabled'] = TRUE;
      $form['amazon_app']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * Ajax callback.
   */
  public function ajaxRefresh(array $form, FormStateInterface $form_state) {
    return $form['marketplace_id'];
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    if ($this->entity->isNew()) {
      $actions['submit_continue'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save and add inventories'),
        '#continue' => TRUE,
        '#submit' => ['::submitForm', '::save'],
      ];
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $amazon_marketplace = $this->entity;

    if (!empty($form_state->getTriggeringElement()['#continue'])) {
      $form_state->setRedirect('entity.commerce_amazon_marketplace.canonical', ['commerce_amazon_marketplace' => $amazon_marketplace->id()]);
    }
    else {
      $form_state->setRedirect('entity.commerce_amazon_marketplace.collection');
    }

    return $result;
  }

}

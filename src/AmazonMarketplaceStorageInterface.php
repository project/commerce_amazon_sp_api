<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the interface for marketplace storage.
 */
interface AmazonMarketplaceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Loads the available marketplaces for the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface[]
   *   The available marketplaces.
   */
  public function loadAvailable(OrderInterface $order): array;

}

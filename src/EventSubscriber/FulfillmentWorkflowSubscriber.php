<?php

namespace Drupal\commerce_amazon_sp_api\EventSubscriber;

use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Move order to specific state based on response.
 */
class FulfillmentWorkflowSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'commerce_amazon_fulfillment.post_transition' => ['onPostTransition'],
    ];
  }

  /**
   * Triggers creating fulfillment order.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   */
  public function onPostTransition(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillmentInterface $fulfillment */
    $fulfillment = $event->getEntity();

    if ($order = $fulfillment->getOrder()) {
      $marketplace = $fulfillment->getMarketplace();
      $configuration = $marketplace->getOrderTypesWorkflow($order->bundle());
      $workflow_enabled = $configuration['workflow'] ?? FALSE;

      if ($workflow_enabled) {
        $fulfillment_transition = $event->getTransition()->getId();
        $order_transition = $configuration[$fulfillment_transition] ?? NULL;
        // Fulfillment transition can be mapped to multiple same order
        // transitions.
        if ($order_transition && $order->getState()->isTransitionAllowed($order_transition)) {
          $order->getState()->applyTransitionById($order_transition);
          $order->save();
        }
      }
    }
  }

}

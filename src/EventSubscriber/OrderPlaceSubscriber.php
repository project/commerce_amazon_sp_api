<?php

namespace Drupal\commerce_amazon_sp_api\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\commerce_amazon_sp_api\Amazon\FulfillmentOrder;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Create Amazon fulfillment order on order placement.
 */
class OrderPlaceSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The fulfillment order.
   */
  protected FulfillmentOrder $fulfillmentOrder;

  /**
   * Constructs a new OrderPlaceSubscriber object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, FulfillmentOrder $fulfillment_order) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fulfillmentOrder = $fulfillment_order;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Execute events as late possible, so that is execute at the end.
    return [
      'commerce_order.place.post_transition' => ['onOrderPlace', -500],
    ];
  }

  /**
   * Triggers creating fulfillment order.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event.
   */
  public function onOrderPlace(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface[] $marketplaces */
    $marketplaces = $this->entityTypeManager->getStorage('commerce_amazon_marketplace')->loadAvailable($order);

    foreach ($marketplaces as $marketplace) {
      if ($marketplace->isEligible($order)) {
        $this->fulfillmentOrder->createOrder($order, $marketplace);
      }
    }
  }

}

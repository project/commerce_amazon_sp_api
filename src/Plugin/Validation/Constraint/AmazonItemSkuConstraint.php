<?php

namespace Drupal\commerce_amazon_sp_api\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Ensures product variation SKU uniqueness.
 *
 * @Constraint(
 *   id = "AmazonItemSku",
 *   label = @Translation("The SKU of the product variation.", context = "Validation")
 * )
 */
class AmazonItemSkuConstraint extends Constraint {

  /**
   * Sku needs to be unique per marketplace.
   */
  public string $message = 'The SKU %sku is already in use for this marketplace and must be unique.';

}

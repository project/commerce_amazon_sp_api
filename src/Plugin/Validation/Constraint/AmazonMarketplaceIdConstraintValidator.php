<?php

namespace Drupal\commerce_amazon_sp_api\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the AmazonMarketplaceIdConstraint constraint.
 */
class AmazonMarketplaceIdConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constraint construct.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$item = $items->first()) {
      return;
    }

    $marketplace_id = $item->value;
    $marketplace_exists = (bool) $this->entityTypeManager->getStorage('commerce_amazon_marketplace')->getQuery()
      ->accessCheck(FALSE)
      ->condition('marketplace_id', (int) $marketplace_id, '=')
      ->range(0, 1)
      ->count()
      ->execute();

    if ($marketplace_exists) {
      $this->context->buildViolation($constraint->message)
        ->setParameter('%marketplace', $this->formatValue($marketplace_id))
        ->addViolation();
    }
  }

}

<?php

namespace Drupal\commerce_amazon_sp_api\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the AmazonItemSkuConstraint constraint.
 */
class AmazonItemSkuConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The route match.
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constraint construct.
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager) {
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint) {
    if (!$item = $items->first()) {
      return;
    }

    $sku = $item->value ?? $item->getPurchasebleEntity()->getSku();
    /** @var \Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface $item */
    $item = $this->routeMatch->getParameter('commerce_amazon_item');
    $marketplace = $this->routeMatch->getParameter('commerce_amazon_marketplace');
    if (isset($sku) && $sku !== '') {
      $entity_query = $this->entityTypeManager->getStorage('commerce_amazon_item')->getQuery();
      $sku_exists = (bool) $entity_query
        ->accessCheck(FALSE)
        ->condition('sku', $sku)
        ->condition('marketplace', $item ? $item->getAmazonMarketplaceId() : $marketplace->getMarketplaceId())
        ->condition('id', (int) $item?->id(), '<>')
        ->range(0, 1)
        ->count()
        ->execute();

      if ($sku_exists) {
        $this->context->buildViolation($constraint->message)
          ->setParameter('%sku', $this->formatValue($sku))
          ->addViolation();
      }
    }
  }

}

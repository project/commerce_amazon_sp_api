<?php

namespace Drupal\commerce_amazon_sp_api\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Ensures marketplace unique.
 *
 * @Constraint(
 *   id = "AmazonMarketplaceId",
 *   label = @Translation("Amazon marketplace id mapped with an app", context = "Validation")
 * )
 */
class AmazonMarketplaceIdConstraint extends Constraint {

  /**
   * The marketplace needs to be unique.
   */
  public string $message = 'The marketplace %marketplace is already in use and must be unique.';

}

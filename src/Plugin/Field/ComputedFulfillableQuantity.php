<?php

namespace Drupal\commerce_amazon_sp_api\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Computed value of fulfillment stock.
 */
final class ComputedFulfillableQuantity extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $entity = $this->getEntity();
    /** @var \Drupal\commerce_amazon_sp_api\Amazon\Inventory $inventory */
    $inventory = \Drupal::service('commerce_amazon_sp_api.inventory');

    // No querying if we're creating entity.
    if (empty($entity->id())) {
      return;
    }
    $stock = $inventory->getBySku($entity->getSku(), $entity->getAmazonMarketplace());
    $this->list[0] = $this->createItem(0, $stock);
  }

}

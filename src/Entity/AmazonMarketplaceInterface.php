<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a marketplace entity type.
 */
interface AmazonMarketplaceInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * {@inheritdoc}
   */
  public function getName(): string;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string;

  /**
   * {@inheritdoc}
   */
  public function getOrderTypes(): array;

  /**
   * {@inheritdoc}
   */
  public function getOrderTypeIds(): array;

  /**
   * {@inheritdoc}
   */
  public function getStores(): array;

  /**
   * {@inheritdoc}
   */
  public function getStoreIds(): array;

  /**
   * {@inheritdoc}
   */
  public function getItemsIds(): array;

  /**
   * {@inheritdoc}
   */
  public function getItems(): array;

  /**
   * {@inheritdoc}
   */
  public function setItems(array $items): static;

  /**
   * {@inheritdoc}
   */
  public function hasItems(): bool;

  /**
   * {@inheritdoc}
   */
  public function addItem(AmazonItemInterface $item): static;

  /**
   * {@inheritdoc}
   */
  public function removeItem(AmazonItemInterface $item): static;

  /**
   * {@inheritdoc}
   */
  public function hasItem(AmazonItemInterface $item): bool;

  /**
   * {@inheritdoc}
   */
  public function getAmazonApp(): ?AmazonAppInterface;

  /**
   * {@inheritdoc}
   */
  public function setAmazonApp(AmazonAppInterface $app): static;

  /**
   * {@inheritdoc}
   */
  public function getMarketplaceId(): ?string;

  /**
   * {@inheritdoc}
   */
  public function setMarketplaceId(string $marketplace_id): static;

  /**
   * {@inheritdoc}
   */
  public function isEligible(OrderInterface $order): bool;

  /**
   * {@inheritdoc}
   */
  public function syncItems(): bool;

}

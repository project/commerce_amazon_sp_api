<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an item entity type.
 */
interface AmazonItemInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Get the parent marketplace.
   */
  public function getAmazonMarketplace(): AmazonMarketplaceInterface;

  /**
   * Get purchasable entity.
   */
  public function getPurchasableEntity(): ?PurchasableEntityInterface;

  /**
   * Set's purchasable entity.
   */
  public function setPurchasableEntity(PurchasableEntityInterface $purchasable_entity): static;

  /**
   * Get purchasable entity id.
   */
  public function getPurchasableEntityId(): int;

  /**
   * Set's purchasable entity id.
   */
  public function setPurchasableEntityId($purchasable_entity_id): static;

  /**
   * Get sku.
   */
  public function getSku(): string;

  /**
   * Set's the sku.
   */
  public function setSku($sku): static;

  /**
   * Get the quantity.
   */
  public function getQuantity(): string;

  /**
   * Sets the quantity.
   */
  public function setQuantity($quantity): static;

  /**
   * Get amazon fulfillment current available quantity.
   */
  public function getFulfillableQuantity(): int;

}

<?php

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Defines the Amazon Fulfillment entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_amazon_fulfillment",
 *   label = @Translation("Amazon fulfillment"),
 *   label_singular = @Translation("Amazon fulfillment"),
 *   label_plural = @Translation("Amazon fulfillments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count fulfillment",
 *     plural = "@count Amazon fulfillments",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "list_builder" = "\Drupal\commerce_amazon_sp_api\AmazonFulfillmentListBuilder",
 *     "storage" = "Drupal\commerce_amazon_sp_api\AmazonFulfillmentStorage",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   base_table = "commerce_amazon_fulfillment",
 *   internal = TRUE,
 *   entity_keys = {
 *     "id" = "fulfillment_id",
 *     "uuid" = "uuid",
 *   },
 *   field_ui_base_route = "entity.commerce_amazon_fulfillment.settings",
 * )
 */
class AmazonFulfillment extends ContentEntityBase implements AmazonFulfillmentInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getOrderId(): ?int {
    return $this->get('order')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrder(): ?OrderInterface {
    return $this->get('order')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getFulfillmentId();
  }

  /**
   * {@inheritdoc}
   */
  public function getFulfillmentId(): ?string {
    return $this->get('fulfillment_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setFulfillmentId($fulfillment_id) {
    $this->set('fulfillment_id', $fulfillment_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarketplace(): ?AmazonMarketplaceInterface {
    return $this->get('marketplace')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setMarketplace(AmazonMarketplaceInterface $amazon_marketplace): static {
    $this->set('marketplace', $amazon_marketplace);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(string $status): static {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): static {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state')->first();
  }

  /**
   * {@inheritdoc}
   */
  public function getShipments(): array {
    $data = [];
    if (!$this->get('shipments')->isEmpty()) {
      $data = $this->get('shipments')->getValue();
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setShipments(array $value): static {
    $this->set('shipments', $value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    if (empty($this->getFulfillmentId())) {
      $fulfillment_id = 'AMA-DC-' . $this->getOrderId();
      $this->set('fulfillment_id', $fulfillment_id);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['fulfillment_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Fulfillment Order ID'))
      ->setDescription(t('The Amazon fulfillment order ID'));

    $fields['order'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order'))
      ->setDescription(t('The order for the fulfillment.'))
      ->setSetting('target_type', 'commerce_order')
      ->setSetting('handler', 'default');

    $fields['marketplace'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Amazon marketplace'))
      ->setDescription(t('The marketplace for the fulfillment.'))
      ->setSetting('target_type', 'commerce_amazon_marketplace')
      ->setSetting('handler', 'default');

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('State'))
      ->setDescription(t('The fulfillment state.'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'state_transition_form',
        'settings' => [
          'require_confirmation' => TRUE,
          'use_modal' => TRUE,
        ],
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setSetting('workflow_callback', ['\Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillment', 'getWorkflowId']);

    $fields['shipments'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Shipments'))
      ->setDescription(t('A serialized array of shipments data.'));

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the fulfillment was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the fulfillment was changed.'));

    return $fields;
  }

  /**
   * Gets the workflow ID for the state field.
   *
   * @param \Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillment $fulfillment
   *   The fulfillment.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(AmazonFulfillment $fulfillment): string {
    // @todo can we have different workflows, does it make sense at all.
    return 'fulfillment_default';
  }

}

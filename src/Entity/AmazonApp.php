<?php

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Amazon app entity type.
 *
 * @ContentEntityType(
 *   id = "commerce_amazon_app",
 *   label = @Translation("Amazon app"),
 *   label_collection = @Translation("Amazon apps"),
 *   label_singular = @Translation("Amazon app"),
 *   label_plural = @Translation("Amazon apps"),
 *   label_count = @PluralTranslation(
 *     singular = "@count app",
 *     plural = "@count apps",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "list_builder" = "Drupal\commerce_amazon_sp_api\AmazonAppListBuilder",
 *      "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\commerce\CommerceEntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\commerce_amazon_sp_api\Form\AmazonAppForm",
 *       "add" = "Drupal\commerce_amazon_sp_api\Form\AmazonAppForm",
 *       "edit" = "Drupal\commerce_amazon_sp_api\Form\AmazonAppForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "commerce_amazon_app",
 *   admin_permission = "administer commerce_amazon_app",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/commerce/amazon/apps",
 *     "add-form" = "/admin/commerce/amazon/apps/add",
 *     "edit-form" = "/admin/commerce/amazon/apps/{commerce_amazon_app}",
 *     "delete-form" =
 *   "/admin/commerce/amazon/apps/{commerce_amazon_app}/delete"
 *   },
 *   field_ui_base_route = "entity.commerce_amazon_app.settings"
 * )
 */
class AmazonApp extends ContentEntityBase implements AmazonAppInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientId(): ?string {
    return $this->get('client_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientSecret(): ?string {
    return $this->get('client_secret')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setClientSecret(string $client_secret): static {
    $this->set('client_secret', $client_secret);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getApplicationId(): ?string {
    return $this->get('application_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getRefreshToken(): ?string {
    return $this->get('refresh_token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setRefreshToken(string $refresh_token): static {
    $this->set('refresh_token', $refresh_token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAccessToken(): ?string {
    return $this->get('access_token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setAccessToken(string $access_token): static {
    $this->set('access_token', $access_token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMode(): ?string {
    return $this->get('mode')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMode(string $mode): static {
    $this->set('mode', $mode);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSellerId(): ?string {
    return $this->get('seller_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSellerId(string $access_token): static {
    $this->set('seller_id', $access_token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getExpiresIn(): ?int {
    return $this->get('expires_in')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setExpiresIn($timestamp): static {
    $this->set('expires_in', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarketplaces(): array {
    $values = $this->get('marketplaces')->getValue();
    return array_map(function ($item) {
      return $item['value'];
    }, $values);
  }

  /**
   * {@inheritdoc}
   */
  public function setMarketplaces(array $marketplaces): static {
    foreach ($marketplaces as $id => $marketplace) {
      // Some internal market ids are being used on Amazon,
      // we need to skip these.
      if (!isset(self::AMAZON_MARKETPLACES[$marketplace])) {
        unset($marketplaces[$id]);
      }
    }
    $this->set('marketplaces', $marketplaces);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('Administration name for the app.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['client_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Client identifier'))
      ->setDescription(t('Client identifier of the app.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['client_secret'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Client secret'))
      ->setDescription(t('Client secret of the app.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['application_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Application ID'))
      ->setDescription(t('Application ID of the app.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['mode'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Mode'))
      ->setDescription(t('Whether the app will run in sandbox or production.'))
      ->setDefaultValue(TRUE)
      ->setRequired(TRUE)
      ->setSetting('on_label', 'Production')
      ->setSetting('off_label', 'Sandbox')
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['seller_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Seller ID'))
      ->setDescription(t('Amazon Seller ID (Merchant token).'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['marketplaces'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Participating marketplaces enabled in the app.'))
      ->setSetting('allowed_values', self::AMAZON_MARKETPLACES)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['region'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('AWS region'))
      ->setSetting('allowed_values', self::AMAZON_REGIONS)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['refresh_token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Refresh token'))
      ->setDescription(t('The LWA refresh token'))
      ->setSetting('max_length', 1024)
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -4,
      ]);

    $fields['access_token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Access token'))
      ->setSetting('max_length', 1024)
      ->setDescription(t('The LWA access token.'));

    $fields['expires_in'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Token expiration time'))
      ->setDescription(t('The time when token expires.'));

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['logging'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Log requests'))
      ->setDefaultValue(FALSE)
      ->setDescription(t('Log all requests'))
      ->setSetting('on_label', 'Enabled')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the Amazon marketplace was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Amazon marketplace was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function isProduction(): bool {
    return $this->getMode();
  }

  /**
   * Get api endpoint per country.
   */
  public function getApiEndpointByCountry(string $country): string {
    $url = match ($country) {
      'US', 'MX', 'CA', 'BR' => 'https://sandbox.sellingpartnerapi-na.amazon.com',
      'SQ', 'AU', 'JP' => 'https://sandbox.sellingpartnerapi-fe.amazon.com',
      default => 'https://sandbox.sellingpartnerapi-eu.amazon.com',
    };

    if ($this->isProduction()) {
      $url = str_replace('sandbox.', '', $url);
    }

    return $url;
  }

  /**
   * Get api endpoint per AWS region.
   */
  public function getEndpointByRegion(): string {
    $url = match ($this->get('region')->value) {
      'us-east-1' => 'https://sandbox.sellingpartnerapi-na.amazon.com',
      'us-west-2' => 'https://sandbox.sellingpartnerapi-fe.amazon.com',
      default => 'https://sandbox.sellingpartnerapi-eu.amazon.com',
    };

    if ($this->isProduction()) {
      $url = str_replace('sandbox.', '', $url);
    }

    return $url;
  }

  /**
   * Get api endpoint per marketplace id.
   */
  public function getApiEndpointById(string $marketplace_id): string {
    return $this->getApiEndpointByCountry(self::AMAZON_MARKETPLACES[$marketplace_id]);
  }

}

<?php

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides interface for AmazonFulfillment entity.
 */
interface AmazonFulfillmentInterface extends ContentEntityInterface {

  /**
   * Gets the order ID.
   */
  public function getOrderId(): ?int;

  /**
   * Gets the order.
   */
  public function getOrder(): ?OrderInterface;

  /**
   * Get the Amazon fulfillment id.
   */
  public function getFulfillmentId(): ?string;

  /**
   * Set the Amazon fulfillment id.
   */
  public function setFulfillmentId(string $fulfillment_id);

  /**
   * Gets the status.
   */
  public function getStatus(): string;

  /**
   * Set the status.
   */
  public function setStatus(string $status): static;

  /**
   * Gets the creation timestamp.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the creation timestamp.
   */
  public function setCreatedTime(int $timestamp): static;

  /**
   * Gets the Amazon app.
   */
  public function getMarketplace(): ?AmazonMarketplaceInterface;

  /**
   * Set the Amazon app team.
   */
  public function setMarketplace(AmazonMarketplaceInterface $amazon_marketplace): static;

  /**
   * Gets a fulfillment shipments data item data value with the given key.
   *
   * @return mixed
   *   The value.
   */
  public function getShipments(): array;

  /**
   * Sets a fulfillment shipments data value with the given key.
   *
   * @param array $value
   *   The value.
   *
   * @return $this
   */
  public function setShipments(array $value): static;

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\commerce\ConditionGroup;
use Drupal\commerce\Entity\CommerceContentEntityBase;
use Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface;
use Drupal\commerce\Plugin\Commerce\Condition\ParentEntityAwareInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the marketplace entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_amazon_marketplace",
 *   label = @Translation("Amazon Marketplace"),
 *   label_collection = @Translation("Amazon Marketplaces"),
 *   label_singular = @Translation("marketplace"),
 *   label_plural = @Translation("marketplaces"),
 *   label_count = @PluralTranslation(
 *     singular = "@count marketplace",
 *     plural = "@count marketplaces",
 *   ),
 *   handlers = {
 *     "storage" = "\Drupal\commerce_amazon_sp_api\AmazonMarketplaceStorage",
 *     "list_builder" =
 *   "Drupal\commerce_amazon_sp_api\AmazonMarketplaceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" =
 *   "Drupal\commerce_amazon_sp_api\AmazonMarketplaceAccessControlHandler",
 *     "form" = {
 *       "default" =
 *   "Drupal\commerce_amazon_sp_api\Form\AmazonMarketplaceForm",
 *       "add" = "Drupal\commerce_amazon_sp_api\Form\AmazonMarketplaceForm",
 *       "edit" = "Drupal\commerce_amazon_sp_api\Form\AmazonMarketplaceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" =
 *   "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *       "integration" =
 *   "Drupal\commerce_amazon_sp_api\Form\AmazonMarketplaceIntegrationForm",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *      },
 *     "route_provider" = {
 *       "html" =
 *   "Drupal\commerce_amazon_sp_api\Routing\AmazonMarketplaceRouteProvider",
 *       "delete-multiple" =
 *   "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *   },
 *   base_table = "commerce_amazon_marketplace",
 *   admin_permission = "administer commerce_amazon_marketplace",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/commerce/amazon/marketplace",
 *     "add-form" = "/admin/commerce/amazon/marketplace/add",
 *     "edit-form" =
 *   "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/edit",
 *     "delete-form" =
 *   "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/delete",
 *     "delete-multiple-form" =
 *   "/admin/commerce/amazon/marketplace/delete-multiple",
 *     "canonical" =
 *   "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}",
 *     "integration-form" =
 *   "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/integration",
 *   },
 *   field_ui_base_route = "entity.commerce_amazon_marketplace.settings",
 * )
 */
class AmazonMarketplace extends CommerceContentEntityBase implements AmazonMarketplaceInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    if ($rel == 'canonical') {
      $rel = 'edit-form';
    }
    return parent::toUrl($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }

    if ($app = $this->getAmazonApp()) {
      $allowed_marketplaces = $app->getMarketplaces();

      if (!in_array($this->getMarketplaceId(), $allowed_marketplaces)) {
        throw new EntityMalformedException(sprintf('The submitted marketplace id %s is not participating in app %s', $this->getMarketplaceId(), $app->getName()));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each item.
    foreach ($this->getItems() as $item) {
      if (!$item->getAmazonMarketplaceId()) {
        $item->set('marketplace', $this->id());
        $item->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName(): string {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderTypes(): array {
    return $this->get('order_types')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderTypes(array $order_types) {
    $this->set('order_types', $order_types);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderTypeIds(): array {
    $order_type_ids = [];
    foreach ($this->get('order_types') as $field_item) {
      $order_type_ids[] = $field_item->target_id;
    }
    return $order_type_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrderTypeIds(array $order_type_ids) {
    $this->set('order_types', $order_type_ids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStores(): array {
    return $this->get('stores')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setStores(array $stores) {
    $this->set('stores', $stores);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStoreIds(): array {
    $store_ids = [];
    foreach ($this->get('stores') as $field_item) {
      $store_ids[] = $field_item->target_id;
    }
    return $store_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function setStoreIds(array $store_ids) {
    $this->set('stores', $store_ids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditions() {
    $conditions = [];
    foreach ($this->get('conditions') as $field_item) {
      /** @var \Drupal\commerce\Plugin\Field\FieldType\PluginItemInterface $field_item */
      $condition = $field_item->getTargetInstance();
      if ($condition instanceof ParentEntityAwareInterface) {
        $condition->setParentEntity($this);
      }
      $conditions[] = $condition;
    }
    return $conditions;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditions(array $conditions) {
    $this->set('conditions', []);
    foreach ($conditions as $condition) {
      if ($condition instanceof ConditionInterface) {
        $this->get('conditions')->appendItem([
          'target_plugin_id' => $condition->getPluginId(),
          'target_plugin_configuration' => $condition->getConfiguration(),
        ]);
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConditionOperator() {
    return $this->get('condition_operator')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setConditionOperator($condition_operator) {
    $this->set('condition_operator', $condition_operator);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled) {
    $this->set('status', (bool) $enabled);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getItemsIds(): array {
    $item_ids = [];
    foreach ($this->get('items') as $field_item) {
      $item_ids[] = $field_item->target_id;
    }
    return $item_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getItems(): array {
    return $this->get('items')->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function setItems(array $items): static {
    $this->set('items', $items);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasItems(): bool {
    return !$this->get('items')->isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function addItem(AmazonItemInterface $item): static {
    if (!$this->hasItem($item)) {
      $this->get('items')->appendItem($item);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeItem(AmazonItemInterface $item): static {
    $index = $this->getItemIndex($item);
    if ($index !== FALSE) {
      $this->get('items')->offsetUnset($item);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasItem(AmazonItemInterface $item): bool {
    return in_array($item->id(), $this->getItemsIds());
  }

  /**
   * Gets the index of the given item.
   *
   * @param \Drupal\commerce_amazon_sp_api\Entity\AmazonItemInterface $item
   *   The fulfillment item.
   *
   * @return int|bool
   *   The index of the given item, or FALSE if not found.
   */
  protected function getItemIndex(AmazonItemInterface $item): bool|int {
    return array_search($item->id(), $this->getItemsIds());
  }

  /**
   * {@inheritdoc}
   */
  public function getAmazonApp(): ?AmazonAppInterface {
    return $this->get('amazon_app')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setAmazonApp(AmazonAppInterface $app): static {
    $this->set('amazon_app', $app);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMarketplaceId(): ?string {
    return $this->get('marketplace_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMarketplaceId(string $marketplace_id): static {
    $this->set('marketplace_id', $marketplace_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function syncItems(): bool {
    return (bool) $this->get('sync_items')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMapping(): array {
    $data = [];
    if (!$this->get('mapping')->isEmpty()) {
      $data = $this->get('mapping')->first()->getValue();
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function setMapping(array $value): static {
    $this->set('mapping', $value);
    return $this;
  }

  /**
   * Get fulfillment policy.
   */
  public function getFulfillmentPolicy(): string {
    $mapping = $this->getMapping();
    return $mapping['fulfillment']['fulfillment_policy'] ?? 'FillAll';
  }

  /**
   * Get shipping speed.
   */
  public function getFulfillmentShippingSpeed(): string {
    $mapping = $this->getMapping();
    return $mapping['fulfillment']['shipping_speed_category'] ?? 'Standard';
  }

  /**
   * Get inventory sync period.
   */
  public function getInventorySyncPeriod(): int {
    $mapping = $this->getMapping();
    return isset($mapping['inventory']['sync_period']) ? (int) $mapping['inventory']['sync_period'] : 900;
  }

  /**
   * Get inventory sync period.
   */
  public function getLowInventoryThreshold(): int {
    $mapping = $this->getMapping();
    return isset($mapping['fulfillment']['inventory_threshold']) ? (int) $mapping['fulfillment']['inventory_threshold'] : 0;
  }

  /**
   * Get order types.
   */
  public function getOrderTypesWorkflow($order_type): array {
    $mapping = $this->getMapping();
    return $mapping['fulfillment']['order_types'][$order_type] ?? [];
  }

  /**
   * Get marketplace assigned country code.
   */
  public function getMarketplaceCountryCode(): string {
    return AmazonAppInterface::AMAZON_MARKETPLACES[$this->getMarketplaceId()];
  }

  /**
   * {@inheritdoc}
   */
  public function isEligible(OrderInterface $order): bool {
    $profiles = $order->collectProfiles();
    $shipping_profile = $profiles['shipping'] ?? NULL;
    // If we have shipping profile, match by country code
    // of profile with Amazon marketplace country code.
    if ($shipping_profile) {
      if (!$shipping_profile->get('address')->isEmpty()) {
        /** @var \Drupal\address\AddressInterface $address */
        $address = $shipping_profile->get('address')->first();
        $shipping_country = $address->getCountryCode();
        if ($shipping_country !== $this->getMarketplaceCountryCode()) {
          return FALSE;
        }
      }
    }

    $conditions = $this->getConditions();
    if (!$conditions) {
      // Marketplace without conditions always apply.
      return TRUE;
    }
    // Filter the conditions just in case there are leftover order item
    // conditions (which have been moved to offer conditions).
    $conditions = array_filter($conditions, function ($condition) {
      /** @var \Drupal\commerce\Plugin\Commerce\Condition\ConditionInterface $condition */
      return $condition->getEntityTypeId() == 'commerce_order';
    });
    $condition_group = new ConditionGroup($conditions, $this->getConditionOperator());

    return $condition_group->evaluate($order);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['order_types'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Order types'))
      ->setDescription(t('The order types for which the marketplace is valid.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSetting('target_type', 'commerce_order_type')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'commerce_entity_select',
        'weight' => 2,
      ]);

    $fields['stores'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Stores'))
      ->setDescription(t('Limit availability to selected stores.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setSetting('target_type', 'commerce_store')
      ->setSetting('handler', 'default')
      ->setSetting('optional_label', t('Restrict to specific stores'))
      ->setDisplayOptions('form', [
        'type' => 'commerce_entity_select',
        'weight' => 2,
      ]);

    $fields['amazon_app'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Amazon App'))
      ->setDescription(t('The parent Amazon app'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'commerce_amazon_app')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ]);

    $fields['marketplace_id'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Marketplace'))
      ->setSetting('allowed_values', AmazonAppInterface::AMAZON_MARKETPLACES)
      ->setRequired(TRUE)
      ->addConstraint('AmazonMarketplaceId')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValue('AND');

    $fields['conditions'] = BaseFieldDefinition::create('commerce_plugin_item:commerce_condition')
      ->setLabel(t('Conditions'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'commerce_conditions',
        'weight' => 3,
        'settings' => [
          'entity_types' => ['commerce_order'],
        ],
      ]);

    $fields['condition_operator'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Condition operator'))
      ->setDescription(t('The condition operator.'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', [
        'AND' => t('All conditions must pass'),
        'OR' => t('Only one condition must pass'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValue('AND');

    $fields['items'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Amazon Items'))
      ->setDescription(t('List of purchase items available on Amazon.'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setRequired(FALSE)
      ->setSetting('target_type', 'commerce_amazon_item')
      ->setSetting('handler', 'default');

    $fields['sync_items'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Sync items from FBA inventory'))
      ->setDescription(t('Create items based of FBA stock inventory.'))
      ->setDefaultValue(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'on_label' => t('Yes'),
        'off_label' => t('No'),
      ])
      ->setSetting('on_label', 'Yes')
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['mapping'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Integration mapping'))
      ->setDescription(t('A serialized array of integration mapping for Commerce core'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the Amazon marketplace was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the Amazon marketplace was last edited.'));

    return $fields;
  }

}

<?php

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\commerce\Entity\CommerceContentEntityBase;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_amazon_sp_api\Plugin\Field\ComputedFulfillableQuantity;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the Amazon item entity.
 *
 * @ContentEntityType(
 *   id = "commerce_amazon_item",
 *   label = @Translation("Amazon item"),
 *   label_collection = @Translation("Amazon items"),
 *   label_singular = @Translation("item"),
 *   label_plural = @Translation("items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count item",
 *     plural = "@count items",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\commerce_amazon_sp_api\AmazonItemStorage",
 *     "list_builder" = "Drupal\commerce_amazon_sp_api\AmazonItemListBuilder",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\commerce\CommerceEntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\commerce_amazon_sp_api\Form\AmazonItemForm",
 *       "edit" = "Drupal\commerce_amazon_sp_api\Form\AmazonItemForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\commerce_amazon_sp_api\Routing\AmazonItemRouteProvider",
 *     },
 *   },
 *   admin_permission = "administer commerce_amazon_marketplace",
 *   base_table = "commerce_amazon_item",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/items/add",
 *     "edit-form" = "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/items/{commerce_amazon_item}/edit",
 *     "delete-form" = "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/items/{commerce_amazon_item}/delete",
 *     "collection" = "/admin/commerce/amazon/marketplace/{commerce_amazon_marketplace}/items",
 *   },
 *   field_ui_base_route = "entity.commerce_amazon_item.settings"
 * )
 */
class AmazonItem extends CommerceContentEntityBase implements AmazonItemInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * Flag to skip API  calls during creation / sync.
   */
  public bool $skipUpdate;

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['commerce_amazon_marketplace'] = $this->getAmazonMarketplaceId();
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    if ($this->isNew()) {
      return '';
    }
    $purchasable_entity = $this->getPurchasableEntity();
    $purchasable_entity_label = $purchasable_entity ? $purchasable_entity->label() : '';

    return sprintf('%s: %s', $purchasable_entity_label, $this->getSku());
  }

  /**
   * {@inheritdoc}
   */
  public function getAmazonMarketplace(): AmazonMarketplaceInterface {
    return $this->get('marketplace')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getAmazonMarketplaceId(): string {
    return $this->get('marketplace')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPurchasableEntity(): ?PurchasableEntityInterface {
    return $this->getTranslatedReferencedEntity('purchasable_entity');
  }

  /**
   * {@inheritdoc}
   */
  public function setPurchasableEntity(PurchasableEntityInterface $purchasable_entity): static {
    return $this->set('purchasable_entity', $purchasable_entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getPurchasableEntityId(): int {
    return $this->get('purchasable_entity')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPurchasableEntityId($purchasable_entity_id): static {
    return $this->set('purchasable_entity', $purchasable_entity_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getSku(): string {
    return $this->get('sku')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setSku($sku): static {
    $this->set('sku', $sku);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuantity(): string {
    return $this->get('quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setQuantity($quantity): static {
    $this->set('quantity', (string) $quantity);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setEnabled($enabled) {
    $this->set('status', (bool) $enabled);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    $marketplace = $this->getAmazonMarketplaceId();
    if (empty($marketplace)) {
      throw new EntityMalformedException(sprintf('Required item field "marketplace" is empty.'));
    }

    if (empty($this->get('sku')->value)) {
      $this->set('sku', $this->getPurchasableEntity()->getSku());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Sandbox specifics.
    if (!$this->getAmazonMarketplace()->getAmazonApp()->isProduction() && empty($this->skipUpdate)) {

      if (!$update) {
        try {
          \Drupal::service('commerce_amazon_sp_api.client')->createInventoryItem($this);
        }
        catch (\Exception) {
          // Do nothing.
        }
      }

      if ((int) $this->getQuantity() > 0) {
        \Drupal::service('commerce_amazon_sp_api.client')->addInventory($this);
        \Drupal::service('commerce_amazon_sp_api.inventory')->updateBySku($this->getSku(), $this->getAmazonMarketplace(), (int) $this->getQuantity());
      }
    }

    // Ensure there's a back-reference on each item.
    if ($marketplace = $this->getAmazonMarketplace()) {
      if (!$marketplace->hasItem($this)) {
        $marketplace->addItem($this);
        $marketplace->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFulfillableQuantity(): int {
    return $this->get('fulfillable_quantity')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $fields['uid']
      ->setLabel(t('Owner'))
      ->setDescription(t('The user that owns this item.'));

    $fields['marketplace'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Amazon marketplace'))
      ->setDescription(t('The parent Amazon marketplace.'))
      ->setSetting('target_type', 'commerce_amazon_marketplace')
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['purchasable_entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Purchasable entity'))
      ->setDescription(t('The purchasable entity.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => -1,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ]);
    // Provide a default target_type for Views, which uses
    // base field definitions without bundle overrides.
    if (\Drupal::moduleHandler()->moduleExists('commerce_product')) {
      $fields['purchasable_entity']->setSetting('target_type', 'commerce_product_variation');
    }

    $fields['sku'] = BaseFieldDefinition::create('string')
      ->setLabel(t('The seller SKU of the item.'))
      ->setDescription(t('The SKU used for specific Amazon marketplace. If empty, system uses default SKU from referenced purchasable entity.'))
      ->setRequired(FALSE)
      ->addConstraint('AmazonItemSku')
      ->setSetting('display_description', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['fulfillable_quantity'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Fulfillable Quantity'))
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setCardinality(1)
      ->setClass(ComputedFulfillableQuantity::class);

    $fields['quantity'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Quantity'))
      ->setDescription(t('The quantity of item.'))
      ->setRequired(FALSE)
      ->setSetting('unsigned', TRUE)
      ->setSetting('min', 0)
      ->setDefaultValue(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'commerce_quantity',
      ]);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('Whether the item is enabled.'))
      ->setDefaultValue(TRUE)
      ->setRequired(TRUE)
      ->setSettings([
        'on_label' => t('Enabled'),
        'off_label' => t('Disabled'),
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
      ]);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the item was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

}

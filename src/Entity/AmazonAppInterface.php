<?php

namespace Drupal\commerce_amazon_sp_api\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining an Amazon app entity type.
 */
interface AmazonAppInterface extends ContentEntityInterface {

  /**
   * Mapped Amazon apps with country and id.
   *
   * @see https://developer-docs.amazon.com/sp-api/docs/marketplace-ids
   */
  public const AMAZON_MARKETPLACES = [
    'A2EUQ1WTGCTBG2' => 'CA',
    'ATVPDKIKX0DER' => 'US',
    'A1AM78C64UM0Y8' => 'MX',
    'A2Q3Y263D00KWC' => 'BR',
    'A1RKKUPIHCS9HS' => 'ES',
    // Amazon uses UK - but we need valid country code, which is GB in this
    // case.
    'A1F83G8C2ARO7P' => 'GB',
    'A13V1IB3VIYZZH' => 'FR',
    'AMEN7PMS3EDWL' => 'BE',
    'A1805IZSGTT6HS' => 'NL',
    'A1PA6795UKMFR9' => 'DE',
    'APJ6JRA9NG5V4' => 'IT',
    'A2NODRKZP88ZB9' => 'SE',
    'AE08WJ6YKNBMC' => 'ZA',
    'A1C3SOZRARQ6R3' => 'PL',
    'ARBP9OOSHTCHU' => 'EG',
    'A33AVAJ2PDY3EV' => 'TR',
    'A17E79C6D8DWNP' => 'SA',
    'A2VIGQ35RCS4UG' => 'AE',
    'A21TJRUUN4KGV' => 'IN',
    'A19VAU5U5O7RUS' => 'SG',
    'A39IBJ37TRP1C6' => 'AU',
    'A1VC38T7YXB528' => 'JP',
  ];

  /**
   * Amazon allowed regions.
   *
   * @see https://developer-docs.amazon.com/amazon-shipping/docs/sp-api-endpoints
   */
  public const AMAZON_REGIONS = [
    'us-east-1' => 'North America (us-east-1)',
    'eu-west-1' => 'Europe (eu-west-1)',
    'us-west-2' => 'Far East (us-west-2)',
  ];

  /**
   * Get the client id.
   */
  public function getClientId(): ?string;

  /**
   * Get client secret.
   */
  public function getClientSecret(): ?string;

  /**
   * Set client secret.
   */
  public function setClientSecret(string $client_secret): static;

  /**
   * Get refresh token.
   */
  public function getRefreshToken(): ?string;

  /**
   * Set refresh token.
   */
  public function setRefreshToken(string $refresh_token): static;

  /**
   * Get access token.
   */
  public function getAccessToken(): ?string;

  /**
   * Set access token.
   */
  public function setAccessToken(string $access_token): static;

  /**
   * Get access token expiration.
   */
  public function getExpiresIn(): ?int;

  /**
   * Set token expiration.
   */
  public function setExpiresIn($timestamp): static;

  /**
   * Get the application id.
   */
  public function getApplicationId(): ?string;

  /**
   * Get the mode.
   */
  public function getMode(): ?string;

  /**
   * Sets the mode.
   */
  public function setMode(string $mode): static;

  /**
   * Get all marketplaces.
   */
  public function getMarketplaces(): array;

  /**
   * Set marketplace values.
   */
  public function setMarketplaces(array $marketplaces): static;

  /**
   * Determine if we're running production mode.
   */
  public function isProduction(): bool;

  /**
   * Get api endpoint per country.
   */
  public function getApiEndpointByCountry(string $country): string;

  /**
   * Get api endpoint per marketplace id.
   */
  public function getApiEndpointById(string $marketplace_id): string;

  /**
   * Get seller id.
   */
  public function getSellerId(): ?string;

  /**
   * Set seller id.
   */
  public function setSellerId(string $access_token): static;

}

<?php

namespace Drupal\commerce_amazon_sp_api;

use Drupal\commerce_amazon_sp_api\Entity\AmazonFulfillmentInterface;
use Drupal\commerce_amazon_sp_api\Entity\AmazonMarketplaceInterface;
use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Provides interface for AmazonFulfillmentStorageInterface.
 */
interface AmazonFulfillmentStorageInterface {

  /**
   * Load fulfillment by order.
   */
  public function loadByOrder(OrderInterface $order): ?AmazonFulfillmentInterface;

  /**
   * Load open fulfillments.
   */
  public function loadOpenFulfillmentIds(AmazonMarketplaceInterface $marketplace): array;

}

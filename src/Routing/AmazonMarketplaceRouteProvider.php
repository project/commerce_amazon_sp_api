<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Routing;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative pages.
 */
final class AmazonMarketplaceRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    if ($review_route = $this->getIntegrationRoute($entity_type)) {
      $collection->add("entity.commerce_amazon_marketplace.integration", $review_route);
    }

    return $collection;
  }

  /**
   * Generate integration form.
   */
  protected function getIntegrationRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('integration-form')) {
      $route = new Route($entity_type->getLinkTemplate('integration-form'));
      $route
        ->addDefaults([
          '_entity_form' => "commerce_amazon_marketplace.integration",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::title',
        ])
        ->setRequirement('_entity_access', 'commerce_amazon_marketplace.update')
        ->setOption('parameters', [
          'commerce_amazon_marketplace' => [
            'type' => 'entity:commerce_amazon_marketplace',
          ],
        ])
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getAddFormRoute($entity_type);
    $route->setDefault('_title_callback', EntityController::class . '::addTitle');
    return $route;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\commerce_amazon_sp_api\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\commerce_amazon_sp_api\Controller\AmazonMarketplaceController;

/**
 * Provides HTML routes for entities with administrative pages.
 */
final class AmazonItemRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getAddFormRoute($entity_type);
    $route->setOption('parameters', [
      'commerce_amazon_marketplace' => [
        'type' => 'entity:commerce_amazon_marketplace',
      ],
    ]);
    $route->setDefault('_title_callback', '');
    $route->setDefault('_title', t('Add item')->getUntranslatedString());

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditFormRoute(EntityTypeInterface $entity_type) {
    $route = parent::getEditFormRoute($entity_type);
    $route->setOption('parameters', [
      'commerce_amazon_marketplace' => [
        'type' => 'entity:commerce_amazon_marketplace',
      ],
    ]);

    return $route;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCollectionRoute(EntityTypeInterface $entity_type) {
    $route = parent::getCollectionRoute($entity_type);
    $route->setOption('parameters', [
      'commerce_amazon_marketplace' => [
        'type' => 'entity:commerce_amazon_marketplace',
      ],
    ]);
    $route->setOption('_admin_route', TRUE);
    $route->setDefault('_title_callback', AmazonMarketplaceController::class . '::amazonMarketplaceTitle');

    return $route;
  }

}

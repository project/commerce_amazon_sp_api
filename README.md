CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Architecture
* Initial setup
* Maintainers

INTRODUCTION
------------
Drupal Commerce Core integration with Amazon Selling Partner API.

https://developer-docs.amazon.com/sp-api/

**Integrated SP API features:**
* Fulfillment Outbound API - https://developer-docs.amazon.com/sp-api/docs/fulfillment-outbound-api-v2020-07-01-reference
* FBA Inventory API - https://developer-docs.amazon.com/sp-api/docs/fbainventory-api-v1-reference

**What this module does?**

Sync stock/inventory from your Amazon Merchant account and link them with
existing product variation inside Drupal.
Set marketplace conditions upon you can create fulfillment orders on Amazon.
Track your Amazon fulfillment order and transition Drupal order based on
status updates from Amazon.

**Is this module what I need?**

If you want to placed orders from Drupal to fulfill with Amazon, this is the module for you.
If you want to manage your Amazon listings, this is not the module for you.

_The module have API calls for Amazon Product listing, but this feature
is not implemented fully within Drupal Commerce._

REQUIREMENTS
------------
This module requires Drupal Commerce Core (v2 or v3) and Commerce Shipping.

ARCHITECTURE
--------------
Drupal module provides four entity types which are interacting with Amazon SP API.

* Amazon App
  * the app which you created under you [Amazon Seller Central](https://sellercentral.amazon.com/sellingpartner/developerconsole/) account.
  * list all available markets from your Amazon merchant account.
  * handles authentication
* Amazon Marketplace
  * the Amazon marketplace by region (as associated with parent Amazon App).
  * references Amazon APP
  * references items from that marketplace with Amazon Item entity.
* Amazon Item
  * the items from Amazon inventory linked to your Drupal variations.
  * Items are listed under Amazon marketplace.
* Amazon Fulfillment
  * the order created on Amazon based of conditions from Amazon Marketplace entity.
  * holds reference to Amazon Marketplace, Commerce Core order

INITIAL SETUP
--------------
* Create SP API type app in Amazon Seller Central (choose Sellers under Business entities)
  * Self authorize your app - copy refresh token
* Create Amazon App inside Drupal - `/admin/commerce/amazon/apps`
  * Fill all token and client data
  * Choose mode
* Create Amazon Marketplace inside Drupal - `/admin/commerce/amazon/marketplace`
  * Choose Amazon App which you created in previous step
  * Choose Marketplace - you can select only one region
  * Leave `Sync items from FBA inventory` checked on if you want to automate inventory sync
  * Select desired conditions upon order placed in Drupal are going to be sent to the Amazon
    to be fulfilled.
* Run cron to initially sync inventory
* Go to general settings - `/admin/commerce/config/amazon-sp-api/settings`
  * Set desired sync period - from 10 to 60 minutes
  * Enable order integration and map states if you want to automate Commerce Core order workflow
    based on Fulfillment status from amazon
* Place an order to test.

MAINTAINERS
-----------
The 1.0.x branch was created by:

* https://www.drupal.org/u/valic
